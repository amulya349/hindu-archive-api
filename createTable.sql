--- The following script should be used to create the table article which will be used.

CREATE TABLE public.article (
	id int8 NOT NULL,
	author varchar(511) NULL,
	description text NULL,
	published_date timestamp NULL,
	title varchar(1023) NULL,
	CONSTRAINT article111_pkey PRIMARY KEY (id)
);

CREATE INDEX article_author_idx ON public.article (author);

CREATE INDEX article_description_idx ON public.article (description,title);

